module bus_arb_tb #(
parameter WIDTH = 'd8 
)(
output reg clk_i                      ,
output reg reset_i                    ,
output reg rq_client0                 ,
output reg rq_client1                 ,
output reg rq_client2                 ,
output reg rq_client3                 ,
output reg [WIDTH -1:0] dataW_client0 ,
output reg [WIDTH -1:0] dataW_client1 ,
output reg [WIDTH -1:0] dataW_client2 ,
output reg [WIDTH -1:0] dataW_client3 ,
output reg wr_client0                 ,
output reg wr_client1                 ,
output reg wr_client2                 ,
output reg wr_client3                 ,

input ack_client0                     ,
input ack_client1                     ,
input ack_client2                     ,
input ack_client3                     


);

initial begin
     clk_i         = 1'b0;
     rq_client0    = 1'b0;
     rq_client1    = 1'b0;
     rq_client2    = 1'b0;
     rq_client3    = 1'b0;
     dataW_client0 =  'b0;
     dataW_client1 =  'b0;
     dataW_client2 =  'b0;
     dataW_client3 =  'b0;
     wr_client0    = 1'b0;
     wr_client1    = 1'b0;
     wr_client2    = 1'b0;
     wr_client3    = 1'b0;

    forever #5 clk_i = ~clk_i;
end

initial begin 
    reset_i      <= 1'b0;
    @(posedge clk_i )   ;  
    reset_i      <= 1'b1;
    @(posedge clk_i )   ;
    reset_i      <= 1'b0;
    @(posedge clk_i )   ;
    rq_client0   <=  'b1;
    wr_client0   <= 1'b0;
    dataW_client0  <='h6;
    @(posedge clk_i )   ;
    @(posedge clk_i )   ;
    rq_client2   <= 1'b1;
    wr_client2   <= 1'b1;
    //dataW_client2 <= 'h10;
    
    @(posedge clk_i )   ;
    rq_client3    <=1'b1;
    wr_client3   <= 1'b1;
    //dataW_client3 <= 'h0;
    
    @(posedge clk_i )   ;
    
end


always @(posedge clk_i or posedge reset_i ) begin   
if(ack_client0) begin 
    rq_client0    <= 'b0;
    dataW_client0 <= 'b0;
end else
if(ack_client1) begin
    rq_client1    <= 'b0;
    dataW_client1 <= 'b0;
end else
if(ack_client2) begin 
    rq_client2    <= 'b0;
    dataW_client2 <= 'b0;
end else
if(ack_client3) begin 
    rq_client3    <= 'b0;
    dataW_client3 <= 'b0;
end end


endmodule
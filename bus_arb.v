module bus_arb #(
parameter WIDTH = 'd8 
)(

input clk_i                           ,  //clk_i f=50MHz
input reset_i                         ,  //reset activ 1
								      
//clienti                             
input rq_client0                      ,  //cerere de la cleintul X, X = 0,1,2,3
input rq_client1                      ,
input rq_client2                      ,
input rq_client3                      ,
input wr_client0                      ,  //Tipul cererii clientului <X> 1=citire, 0=scriere.
input wr_client1                      ,
input wr_client2                      ,
input wr_client3                      ,
input [WIDTH -1:0] dataW_client0      ,  //Date scrise preluate de la clientul <X>.
input [WIDTH -1:0] dataW_client1      ,
input [WIDTH -1:0] dataW_client2      ,
input [WIDTH -1:0] dataW_client3      ,

output reg ack_client0                ,  //Confirmare pentru clientul <X>.
output reg ack_client1                ,
output reg ack_client2                ,
output reg ack_client3                ,
output reg [WIDTH -1:0] dataR_client0 ,  //Date citite livrate către clientul <X>.
output reg [WIDTH -1:0] dataR_client1 ,
output reg [WIDTH -1:0] dataR_client2 ,
output reg [WIDTH -1:0] dataR_client3 ,

//server
input ack_i                           ,  //Confirmare de la server, un puls de o perioadă de tact, activă in 1
input[WIDTH -1:0] dataR_i             ,  //Date citite (include toate informațiile de la server spre client, ca urmare a satisfacerii cererii).

output reg rq_o                       ,  //Cerere spre server, activă în 1, stabilă până la primirea confirmării.
output reg wr_no                      ,  //Tipul cererii 1=citire, 0=scriere.
output reg[WIDTH -1:0] dataW_o           //Date scrise (include toate informațiile de la client spre server, necesare pentru satisfacerea cererii).
);

reg[1:0] current_client;                 //reg pe 2 biti pt clientul curent
reg state;                               //reg pe 1 bit pt starea 0 si 1(idle si running)
reg ackReceived;                         //reg care stie cand a fost primit ack de la server
parameter IDLE=1'b0, RUNNING=1'b1;       //initializarea parametrilor


always @(posedge clk_i or posedge reset_i )begin
if (reset_i) begin                       //initializarea semnalelor la reset
            rq_o             <= 1'b0          ;
            wr_no            <= 1'b0          ;
            ack_client0      <= 1'b0          ;
			ack_client1      <= 1'b0          ;
			ack_client2      <= 1'b0          ;
			ack_client3      <= 1'b0          ;
			current_client   <= 2'b0          ;
            dataR_client0    <= 'b0           ;
			dataR_client1    <= 'b0           ;
			dataR_client2    <= 'b0           ;
			dataR_client3    <= 'b0           ;
			dataW_o          <= 'b0           ;
end else
if(state == RUNNING) begin
    case(current_client)
//client0 
    2'b00 : begin 
	        ack_client0      <= ack_i         ;  
			ack_client1      <= 1'b0          ;
			ack_client2      <= 1'b0          ;
			ack_client3      <= 1'b0          ;
			dataR_client0    <= dataR_i       ;
			dataR_client1    <= 'b0           ;
			dataR_client2    <= 'b0           ;
			dataR_client3    <= 'b0           ;
	        wr_no            <= wr_client0    ; 
            dataW_o          <= dataW_client0 ; 
                if(ack_i) begin 
	        	     rq_o    <= 'b0           ; //cand am ack_i, rq_o trebuie sa coboare
	                 dataW_o <= 'b0           ;                end
                else if(ackReceived == 0) rq_o <= rq_client0 ; end  //req de la client va fi dat mai departe catre server
	        
           			
//client1		
    2'b01 : begin
            ack_client0       <= 1'b0         ;  
    		ack_client1       <= ack_i        ;
    		ack_client2       <= 1'b0         ;
    		ack_client3       <= 1'b0         ;
    		dataR_client0     <= 'b0          ;
    		dataR_client1     <= dataR_i      ;
    		dataR_client2     <= 'b0          ;
    		dataR_client3     <= 'b0          ;
    		wr_no             <= wr_client1   ;
    		dataW_o           <= dataW_client1;
				if(ack_i) begin 
				    rq_o      <= 'b0          ; //cand am ack_i, rq_o trebuie sa coboare
				    dataW_o   <= 'b0          ; end
           	    else if(ackReceived == 0) rq_o <= rq_client1   ; end
//client2
    2'b10 : begin
            ack_client0      <= 1'b0          ;
   			ack_client1      <= 1'b0          ;
   			ack_client2      <= ack_i         ;
   			ack_client3      <= 1'b0          ;
   			dataR_client0    <= 'b0           ;
   			dataR_client1    <= 'b0           ;
   			dataR_client2    <= dataR_i       ;
   			dataR_client3    <= 'b0           ;
   			wr_no            <= wr_client2    ;
   			dataW_o          <= dataW_client2 ;
				if(ack_i) begin 
				     rq_o    <= 'b0           ; //cand am ack_i, rq_o trebuie sa coboare
					 dataW_o <= 'b0           ; end
           	    else if(ackReceived == 0) rq_o <= rq_client2       ; end
//client3			
    2'b11 : begin
            ack_client0      <= 1'b0          ;  
            ack_client1      <= 1'b0          ;
            ack_client2      <= 1'b0          ;
            ack_client3      <= ack_i         ;
            dataR_client0    <= 'b0           ;
            dataR_client1    <= 'b0           ;
            dataR_client2    <= 'b0           ;
            dataR_client3    <= dataR_i       ;
            wr_no            <= wr_client3    ;
            dataW_o          <= dataW_client3 ;
				if(ack_i) begin 
					 rq_o    <= 'b0           ; //cand am ack_i, rq_o trebuie sa coboare
					 dataW_o <= 'b0           ; end
           	    else if(ackReceived == 0) rq_o <= rq_client3    ; end
           endcase end end

always @(posedge clk_i or posedge reset_i )begin
if(reset_i) current_client <= IDLE; else //la reset starea initiala a lui current_client va fi idle
case(state)
	  IDLE  :    if(rq_client0) current_client <= 'b00; else   
           	     if(rq_client1) current_client <= 'b01; else
	             if(rq_client2) current_client <= 'b10; else
			     if(rq_client3) current_client <= 'b11;    
					
	RUNNING :   if(ack_i) begin                                  
	                case(current_client)                     //verific urmatorul client care are req
                    2'b00 : if(rq_client1) current_client <= 'b01; else   
	                        if(rq_client2) current_client <= 'b10; else
	  	             	    if(rq_client3) current_client <= 'b11;     
	  	             	 
                    2'b01 : if(rq_client2) current_client <= 'b10; else
	                        if(rq_client3) current_client <= 'b11; else
	  	             	    if(rq_client0) current_client <= 'b00; 
	  	             	 
                    2'b10 : if(rq_client3) current_client <= 'b11; else
	                        if(rq_client0) current_client <= 'b00; else
	  	             	    if(rq_client1) current_client <= 'b01; 
	  	             	 
                    2'b11 : if(rq_client0) current_client <= 'b00; else
	                        if(rq_client1) current_client <= 'b01; else
	  	            	    if(rq_client2) current_client <= 'b10; 
	                endcase	end 
endcase end 

always @(posedge clk_i or posedge reset_i )begin
if(reset_i) state <= 1'b0; else
case(state)
	//in  idle se verifica daca clientii au facut req
	IDLE:   if(rq_client0 || rq_client1 || rq_client2 || rq_client3)  //portiune care verifica daca exista req de la clienti sa treaca in starea running
			    state <= RUNNING;
	RUNNING:if(!rq_client0 && !rq_client1 && !rq_client2 && !rq_client3)  //portiune care verifica daca exista req de la clienti sa treaca in starea running
			    state <= IDLE;
endcase

end


always @(posedge clk_i or posedge reset_i )begin
if(reset_i) ackReceived <= 1'b0; else
if(ack_i)   ackReceived <= 1'b1; else                         //ackReceived = 1 cand acki este 1, cand ack din server e 1
if(ack_client0 || ack_client1 || ack_client2 || ack_client3)	
            ackReceived <= 1'b0;                             //ackReceived pus in 0 dupa ce s-a facut ack catre client
end

endmodule
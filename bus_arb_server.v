module bus_arb_server #(
parameter WIDTH ='d8
)(
input  clk_i                    ,
input  reset_i                  ,
input  req_i                    , //req_o
input  wr_ni                    , //wr_no
input  [WIDTH -1:0] dataW_o     ,

output reg ack_o                , //ack_i
output reg [WIDTH -1:0] dataR_o   //dataR_i
);

always @(posedge clk_i or posedge reset_i ) begin	
if(reset_i)begin
    ack_o   <= 'b0;
	dataR_o <= 'b0; end else
if(req_i) begin
    if(wr_ni) begin 
	   ack_o  <= 'b1;
	   dataR_o<= 'hA; end else
	if(~wr_ni) begin
	   ack_o  <= 'b1; end  end
if(ack_o)begin
          ack_o   <= 'b0;
          dataR_o <= 'b0;  end  
end

endmodule
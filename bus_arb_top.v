module bus_arb_top              ;
parameter WIDTH = 'd8           ;

wire    clk_i                   ;
wire    reset_i                 ;
wire    rq_client0              ;
wire    rq_client1              ;
wire    rq_client2              ;
wire    rq_client3              ;
wire    wr_client0              ;
wire    wr_client1              ;
wire    wr_client2              ;
wire    wr_client3              ;
wire[WIDTH -1:0] dataW_client0  ;
wire[WIDTH -1:0] dataW_client1  ;
wire[WIDTH -1:0] dataW_client2  ;
wire[WIDTH -1:0] dataW_client3  ;
wire ack_client0                ;
wire ack_client1                ;
wire ack_client2                ;
wire ack_client3                ;
wire [WIDTH -1:0] dataR_client0 ;
wire [WIDTH -1:0] dataR_client1 ;
wire [WIDTH -1:0] dataR_client2 ;
wire [WIDTH -1:0] dataR_client3 ;
wire ack_i                      ;
wire [WIDTH -1:0] dataR_i       ;
wire rq_o                       ;
wire wr_no                      ;
wire [WIDTH -1:0] dataW_o       ;
wire [WIDTH -1:0] dataR_o       ;

bus_arb #(
.WIDTH      (WIDTH    )
) bus_arb_DUT (
.clk_i         (clk_i        )  ,
.reset_i       (reset_i      )  ,
.rq_client0    (rq_client0   )  ,
.rq_client1    (rq_client1   )  ,
.rq_client2    (rq_client2   )  ,
.rq_client3    (rq_client3   )  ,
.wr_client0    (wr_client0   )  ,
.wr_client1    (wr_client1   )  ,
.wr_client2    (wr_client2   )  ,
.wr_client3    (wr_client3   )  ,
.dataW_client0 (dataW_client0)  ,
.dataW_client1 (dataW_client1)  ,
.dataW_client2 (dataW_client2)  ,
.dataW_client3 (dataW_client3)  ,
.ack_client0   (ack_client0  )  ,
.ack_client1   (ack_client1  )  ,
.ack_client2   (ack_client2  )  ,
.ack_client3   (ack_client3  )  ,
.dataR_client0 (dataR_client0)  ,
.dataR_client1 (dataR_client1)  ,
.dataR_client2 (dataR_client2)  ,
.dataR_client3 (dataR_client3)  ,
.ack_i         (ack_i        )  ,
.rq_o          (rq_o         )  ,
.wr_no         (wr_no        )  ,
.dataW_o        (dataW_o     )  ,
.dataR_i       (dataR_i      ) 
);

bus_arb_tb
i_bus_arb (
.clk_i         (clk_i        )  ,
.reset_i       (reset_i      )  ,
.rq_client0    (rq_client0   )  ,
.rq_client1    (rq_client1   )  ,
.rq_client2    (rq_client2   )  ,
.rq_client3    (rq_client3   )  ,
.wr_client0    (wr_client0   )  ,
.wr_client1    (wr_client1   )  ,
.wr_client2    (wr_client2   )  ,
.wr_client3    (wr_client3   )  ,
.dataW_client0 (dataW_client0)  ,
.dataW_client1 (dataW_client1)  ,
.dataW_client2 (dataW_client2)  ,
.dataW_client3 (dataW_client3)  ,
.ack_client0   (ack_client0  )  ,
.ack_client1   (ack_client1  )  ,
.ack_client2   (ack_client2  )  ,
.ack_client3   (ack_client3  )  



);

bus_arb_server 
i_bus_arb_server(
.clk_i         (clk_i        )  ,
.reset_i       (reset_i      )  ,
.req_i         (rq_o         )  ,
.wr_ni         (wr_no        )  ,
.dataW_o       (dataW_o      )  ,
.ack_o         (ack_i        )  ,
.dataR_o       (dataR_i      )

);

endmodule